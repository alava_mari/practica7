package com.facci.practicasiete4d;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{
    Button botonFragment1, botonFragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);
        botonFragment1 = (Button) findViewById(R.id.btnFrgUno);
        botonFragment2 = (Button) findViewById(R.id.btnFrgDos);
        botonFragment1.setOnClickListener(this);
        botonFragment2.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcionVibrar:
                Intent intent = new Intent (ActividadPrincipal.this, ActividadVibrar.class);
                startActivity(intent);
                break;
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(ActividadPrincipal.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button botonAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario = (EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogoLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(ActividadPrincipal.this, cajaUsuario.getText().toString() + " " +
                                cajaClave.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                break;

        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
                FragUno fragmentoUno = new FragUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FragDos fragmentoDos = new FragDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmentoDos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
